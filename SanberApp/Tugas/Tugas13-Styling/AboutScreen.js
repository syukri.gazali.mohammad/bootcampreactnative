import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

export default function About (){
    return(
        <View style={styles.mainContainer}>
            <View style={styles.nameTittle}>
                <View style={{flex:1,justifyContent: 'center', alignItems: 'center',}}>
                    <View>
                        <Image  style={{borderRadius:12,}} source={require('./assets/foto-pp.jpg')} />
                    </View>
                    <View style={{height:16,}}></View>
                    <Text style={{fontWeight:'bold', fontSize: 22,color: 'white',}}>Syukri Gazali Mohammad</Text>
                    <View style={{height:3,}}></View>
                    <Text style={{ fontSize: 16,color: 'white',}}>React Native Programer</Text>
                </View>
            </View>
            <View style={styles.sosmedWork}>
                <View style={styles.box}>
                    <Text  style={styles.boxTittle}>Social media</Text>
                    <View style={{height:10}}></View>
                    <View style={{ flexDirection: 'row', justifyContent:'space-between'}}>
                        <View style={{flexDirection: 'row'}}>
                            <View>
                                <Image source={require('./assets/sosmed-icon-fb.png')}/>
                            </View>
                            <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                        </View>
                        <View style={{ flexDirection: 'row'}}>
                            <View><Image source={require('./assets/sosmed-icon-linkedin.png')}/></View>
                            <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                        </View>
                    </View>
                    <View style={{height:10}}></View>
                    <View style={{ flexDirection: 'row',  justifyContent:'space-between'}}>
                        <View style={{ flexDirection: 'row',}}>
                            <View>
                                <Image source={require('./assets/sosmed-icon-twitter.png')}/>
                            </View>
                            <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                        </View>
                        <View style={{ flexDirection: 'row',}}>
                            <View><Image source={require('./assets/sosmed-icon-ig.png')}/></View>
                            <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                        </View>
                    </View>
                </View>
                <View style={{height:10,}}></View>
                <View style={styles.box}>
                    <Text  style={styles.boxTittle}>My Work</Text>
                    <View style={{height:10}}></View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Image source={require('./assets/mywork-icon-gitlab.png')} />
                        <Text style={{marginLeft:10,fontSize:16, fontWeight:'bold'}}>Gitlab</Text>
                    </View>
                    <Text>https://gitlab.com/syukri.gazali.mohammad/bootcampreactnative</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex:1,
        marginTop:30,
    },
    nameTittle :{
        flex: 1.2,
        backgroundColor: '#6A6FEA',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sosmedWork :{
        flex: 2,
        paddingTop: 20,
    },
    box :{
        flex :1 ,
        borderRadius: 20,
        marginHorizontal: 40,
        marginVertical: 10,
        paddingHorizontal: 20,
        paddingVertical: 10,
        height: 100,
        borderWidth: 1,
        backgroundColor: 'white',
        shadowColor : '#000000',

    },
    boxTittle :{
        fontSize: 24,
        fontWeight: 'bold'
    },
    sosmedText :{
        fontSize:16,
        marginLeft:5,
    },
})