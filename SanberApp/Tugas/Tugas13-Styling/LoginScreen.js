import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

export default function Login (){
    const [value, onChangeText] = React.useState('Masukkan email disini');
    const [value1, onChangeText1] = React.useState('Masukkan password disini');


    return(
        <View style={styles.mainContainer}>
            <View style={styles.logoContainer}>
                <Image 
                    source={require('./assets/logo-depan.jpg')}
                />
                <Text style={styles.namaApp}>Personal Portofolio</Text>
            </View>
            <View style={styles.inputContainer}>
                <View>
                    <Text style={styles.labelInput}>Email</Text>
                    <TextInput
                        style={styles.inputBox}
                        onChangeText={text => onChangeText(text)}
                        value={value}
                    />
                </View>
                <View style={{height: 20,}}></View>
                <View>
                    <Text style={styles.labelInput}>Password</Text>
                    <TextInput
                        style={styles.inputBox}
                        onChangeText1={text => onChangeText1(text)}
                        value1={value1}
                    />
                </View>
                <View style={{flex:1, alignItems:'flex-end'}}>
                    <Text style={{fontWeight: 'bold',}}>Forgot Password ?</Text>
                </View>
                
            </View>
            <View style={styles.buttonContainer}>

                <TouchableOpacity style={styles.button}>
                    <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
                        <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold',}}>Login</Text>
                    </View>
                </TouchableOpacity>
                <View style={{height:10,}}></View>
                <Text style={{textDecorationLine: 'underline', fontWeight: 'bold',}}>Belum punya akun ? Register disini</Text>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex:1,
        marginTop:30,
    },
    logoContainer: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    namaApp:{
        fontSize: 26,
        fontWeight: 'bold',
        color: '#4B4A6D',
        marginTop: 15,
    },
    inputContainer:{
        flex: 1.5,
        paddingHorizontal: 40,
        justifyContent: 'center',
        // backgroundColor: 'blue',
    },
    labelInput:{
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    inputBox:{
        height: 40,
        color: '#E0E0E0',
        borderColor: '#E0E0E0',
        paddingHorizontal: 10,
        marginBottom:10,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
        borderLeftWidth: 0,
        
    },
    buttonContainer :{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button :{
        height: 54,
        width: 320,
        borderRadius:25,
        backgroundColor: '#4B4A6D',
    },  

})