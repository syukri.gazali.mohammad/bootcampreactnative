//===== If-else ===============================================================================================
var nama = "Junaedi"
var peran = "Werewolf"

if (nama == " " && peran == " ") {
    console.log("Nama harus diisi!")
}

if (nama =="John" && peran ==" ") {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
}

if (nama == "Jane" && peran == " ") {
    console.log("Selamat datang di Dunia Werewolf, Jane")   
} else if (nama == "Jane" && peran == "Penyihir") {
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}

if (nama == "Jenita" && peran == " ") {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
} else if (nama == "Jenita" && peran == "Guard") {
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}

if (nama == "Junaedi" && peran == " ") {
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
} else if (nama == "Junaedi" && peran == "Werewolf"){
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}


//===== Switch Case ===============================================================================================

var tanggal = 12;
var bulan = 4;
var tahun = 2002;

switch(tanggal) {
    case 1:   { console.log('1'); break; }
    case 2:   { console.log('2'); break; }
    case 3:   { console.log('3'); break; }
    case 4:   { console.log('4'); break; }
    case 5:   { console.log('5'); break; }
    case 6:   { console.log('6'); break; }
    case 7:   { console.log('7'); break; }
    case 8:   { console.log('8'); break; }
    case 9:   { console.log('9'); break; }
    case 10:   { console.log('10'); break; }
    case 11:   { console.log('11'); break; }
    case 12:   { console.log('12'); break; }
    case 13:   { console.log('13'); break; }
    case 14:   { console.log('14'); break; }
    case 15:   { console.log('15'); break; }
    case 16:   { console.log('16'); break; }
    case 17:   { console.log('17'); break; }
    case 18:   { console.log('18'); break; }
    case 19:   { console.log('19'); break; }
    case 20:   { console.log('20'); break; }
    default:  { console.log('Tanggal hanya 1 sampai 31'); }}


switch(bulan) {
    case 1:   { console.log('Januari'); break; }
    case 2:   { console.log('Februari'); break; }
    case 3:   { console.log('Maret'); break; }
    case 4:   { console.log('April'); break; }
    case 5:   { console.log('Mei'); break; }
    case 6:   { console.log('Juni'); break; }
    case 7:   { console.log('Juli'); break; }
    case 8:   { console.log('Agustus'); break; }
    case 9:   { console.log('September'); break; }
    case 10:   { console.log('Oktober'); break; }
    case 11:   { console.log('November'); break; }
    case 12:   { console.log('Desember') ; break; }
    default:  { console.log("1 tahun hanya 12 bulan"); }}


switch(tahun) {
    case 2000:   { console.log('2000'); break; }
    case 2001:   { console.log('2001'); break; }
    case 2002:   { console.log('2002'); break; }
    case 2003:   { console.log('2003'); break; }
    case 2004:   { console.log('2004'); break; }
    case 2005:   { console.log('2005'); break; }
    default:  { console.log('Tanggal hanya 1 sampai 31'); }}

