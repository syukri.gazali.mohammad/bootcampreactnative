
// Soal no 1 ========================================================================================
console.log('LOOPING PERTAMA')

var flag = 2;
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log( flag + ' - I love coding' ); // Menampilkan nilai flag pada iterasi tertentu
  flag += 2; // Mengubah nilai flag dengan menambahkan 1
}


console.log('LOOPING KEDUA')

var flag = 20;
while(flag >= 2) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log( flag + ' - I will become a mobile developer' ); // Menampilkan nilai flag pada iterasi tertentu
  flag -= 2; // Mengubah nilai flag dengan menambahkan 1
}

// Soal no 2 ========================================================================================

var santai = ' - Santai'
var berkualitas = ' - Berkualitas'
var love = ' - I love coding'

for (angka = 1;angka <= 20; angka++){
    if(angka % 2 != 1){
        console.log(angka + berkualitas)
    } else if(angka % 3 == 0){
        console.log(angka + love)
    } else {
        console.log(angka + santai)
    }
}

// Soal no 3 ========================================================================================

var a = 1
var b = 1
var panjang = 8
var lebar = 4
var pagar = ''

while(b <= lebar){
    while(a <= panjang){
        pagar += '#'
        a++
    }
    console.log(pagar)
    pagar = ''
    a = 1
    b++
}

// Soal no 4 ========================================================================================

var a = 1
var b = 1
var alas = 7
var tinggi = 7
var pagar = ''

for(a = 1;a <= tinggi;a++){
    for(b = 1;b <= a;b++){
        pagar +='#'
    }
    console.log(pagar)
    pagar = ''
}

// Soal no 5 ========================================================================================

var a = 1
var b = 1
var panjang = 8
var lebar = 8
var papan = ''

for( b = 1 ; b <= lebar ; b++){
    if(b%2 == 1){
        for(a=1 ; a <= panjang ; a++){
            if(a%2 == 1){
                papan +=' '
            }else{
                papan +='#'
            }
        }
    }else {
        for( a=1 ; a<= panjang ; a++){
            if(a%2 == 1){
                papan +='#'
            }else{
                papan +=' '
            }
        }
    }
    console.log(papan)
    papan = ''
}